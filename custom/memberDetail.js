var userInfo = {};

$(document).ready(()=>{
	
});

var mainApp = angular.module("memberList", []);
mainApp.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
		if(a[field].toLowerCase() < b[field].toLowerCase()) return -1;
		if(a[field].toLowerCase() > b[field].toLowerCase()) return 1;
		return 0;
	
	});
    if(reverse) filtered.reverse();
    return filtered;
  };
});

mainApp.filter('custom', function() {
  return function(input, search) {
	if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
		var status = true;
		angular.forEach(search, function(value2, key2){
			try{
				var actual = value[key2];
				if (!(actual.toString().toLowerCase().indexOf(value2.toString().toLowerCase()) !== -1)) {
					status = false;
				}
			}catch(e){
				status = false;
			}
			
		});
		if(status){
			result[key] = value;
		}
      /*if(value.fullname.toLowerCase()=="Jose"){
		  console.log(actual.indexOf.toLowerCase()(expected));
		}
	  if (actual.toLowerCase().indexOf(expected) !== -1) {
        result[key] = value;
      }*/
    });
    return result;
  }
});


mainApp.controller('listController', function($scope, $filter) {
	$scope.customDate = '';
	$scope.data= {};
	$scope.key= '';
	
	$scope.filterData = {};
	$scope.status = true;
	
	$scope.updateField = function(id){
		$('.loading').show();
		var ref=firebase.database().ref('OltParticipan/' + $scope.key);
		ref.update({
			statusConf:id
		}).then(function(){
			$('.loading').hide();
			window.location = 'Mlist.html';
		}).catch(function(er){
			console.log(er);
			$('.loading').hide();
		});
		
		
	};
	
	$scope.getStatus = function(id){
		var tmp = '';
		if(id==2){
			tmp = 'Terima';
		}else if(id==3){
			tmp = 'Tolak';
		}else if(id===1){
			tmp = 'Konfirmasi';
		}else if(id===0){
			tmp = 'Baru';
		}
		return tmp;
	};
	
	$scope.getKategori = function(nominal){
		if(nominal == 400000){
			return 'Early Bird';
		}else{
			return 'Regular';
		}
	}
	
	$scope.load = function(){
		$scope.data = JSON.parse(localStorage.memberInfo);
		$scope.key = $scope.data.key;
		firebase.database().ref('OltParticipan').child($scope.key).once("value", function(snap){
			$scope.data  = snap.val();
			$scope.$apply();
		});
		delete $scope.data.key;
		$('.loading').hide();
	};
	$scope.load();
	$scope.resentEmail = function(){
		$.ajax({
			url:"https://us-central1-i-hub-favour.cloudfunctions.net/resentEmailYc?id=" + $scope.key,
			method:"get",
			dataType: "json"
		}).done(function(msg){
			console.log(msg);
			alert("Email Sent");
		});
	};
	
	$scope.absen = function(){
		var x = confirm("Apakah anda yakin untuk mengabsen?");
		if(x){
			console.log($scope.key)
			firebase.database().ref('OltParticipan').child($scope.key).update({absen:'OK'}, function(err){
				if(err){
					console.log(err);
				}else{
					$scope.load();
					alert("Berhasil Absen");
				}
			});
			
		}else{
			alert("");
		}
	};
	
	
	$scope.batalkan = function(){
		var x = confirm("Apakah anda yakin untuk membatalkan absen?");
		if(x){
			console.log($scope.key)
			firebase.database().ref('OltParticipan').child($scope.key).update({absen:'NO'},function(err){
				if(err){
					console.log(err);
				}else{
					$scope.load();
					alert("Berhasil membatalkan");
				}
			});
			
		}else{
			alert("Transaksi dibatalkan");
		}
	};
	$scope.resetPay2 = function(){
		var x = confirm("Apakah anda yakin untuk mereset pembayaran pertama ini?");
		if(x){
			console.log($scope.key)
			firebase.database().ref('OltParticipan').child($scope.key).child('payment').update({secondPayment:null}, function(err){
				if(err){
					console.log(err);
				}else{
					$scope.load();
					alert("Berhasil mereset pembayaran");
				}
			});
			
		}else{
			alert("Transaksi dibatalkan");
		}
	};
	$scope.acceptPay2 = function(){
		var x = confirm("Apakah anda yakin untuk menerima pembayaran pertama ini?");
		if(x){
			console.log($scope.key)
			firebase.database().ref('OltParticipan').child($scope.key).child('payment').update({secondPayment:Date.now()}, function(err){
				if(err){
					console.log(err);
				}else{
					$scope.load();
					alert("Berhasil menerima pembayaran");
				}
			});
			
		}else{
			alert("Transaksi dibatalkan");
		}
	};
 });
var access = {};
firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		authId = user.uid;
		firebase.database().ref('users/' + authId).on('value', (snap)=>{
//			if(true)){
				firebase.database().ref('users/' + authId).set({
					email: user.email, 
					fullName: user.displayName,
					phone:user.phoneNumber,
					profileUrl:user.photoURL,
					access:{
						onlife:true
					}
				});
			
		});
	} else {
		window.location ='login.html';
	}
});
