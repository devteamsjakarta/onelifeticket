'use strict';
var globalFields=[];
var docsCode = '';
const createField = (data) => {
	const {name, label, type, custom, action, validation, Cssclass, options} = data;
	switch(type){
		case 'text':
			return addText(name, label,validation);
			break;
		case 'number':
			return addNumberText(name, label,validation);
			break;
		case 'date':
			return addDate(name, label,validation);
			break;
		case 'textArea':
			return addTextArea(name, label,validation);
			break;
		case 'select':
			return addSelect(name, label,validation, options);
			break;
		case 'custom':
			return addCustom(custom);
			break;
		case 'button':
			let tmpClass = '';
			if(Cssclass==undefined){
				tmpClass="btn btn-primary";
			}else{
				tmpClass = Cssclass;
			}
			return addButton(name, label, action, tmpClass);
			break;
				
	}
};

const onLeaveRequeired = (e) =>{
	var name = $(e).attr('name');
	if(e.value===''){
		$('#'+name +'Group label').css('color','red').notify(
			"Bagian ini harus terisi", 
			{ 
			className:"error",
			position:"right" }
		);
	}else{
		$('#'+name +'Group label').css('color','green');
	}
}

const addSelect = (name, label, validation, options) => {
	var tmp ='';
	$.each(options, (key, value)=>{
		tmp += '<option value="'+ value.value +'">'+ value.label +'</option>'
	});
	
	return '<div id="'+ name +'Group" class="form-group"> \
				<label for="'+ name +'">'+ label +'</label> \
				<select id="'+ name +'" name="'+ name +'" class="form-control select2">\
				'+ tmp +' \
				</select> \
			</div>';
}

const doValidate = (validation) =>{
	var onBlur ='';
	var tmp = validation.split(';');
	$.each(tmp, (key,value) => {
		switch(value) {
			case 'required':
				onBlur += 'onLeaveRequeired(this)';
				break;
		}
	});
	return onBlur;
};

const addText = (name, label, validation) => {
	var onBlur= doValidate(validation);
	return '<div id="'+ name +'Group" class="form-group"> \
				<label for="'+ name +'">'+ label +'</label> \
				<input onblur="'+ onBlur +'" type="text" class="form-control" id="'+name+'" name="'+name+'"> \
			</div>';
}

const addNumberText = (name, label,validation) => {
	var onBlur= doValidate(validation);
	return '<div id="'+ name +'Group" class="form-group"> \
					<label for="'+ name +'">'+ label +'</label> \
					<input onblur="'+ onBlur +'" type="number" class="form-control" id="'+name+'" name="'+name+'"> \
				</div>';
}

const addTextArea = (name, label, validation) => {
	var onBlur= doValidate(validation);
	return '<div id="'+ name +'Group" class="form-group"> \
					<label for="'+ name +'">'+ label +'</label> \
					<textarea onblur="'+ onBlur +'" id="'+ name +'" name="'+name+'" class="form-control"></textarea>\
				</div>';
}

const addDate = (name, label,validation) => {
	var onBlur= doValidate(validation);
	return '<div id="'+ name +'Group" class="form-group"> \
					<label for="'+ name +'">'+ label +'</label> \
					<input onblur="'+ onBlur +'"  type="date" class="form-control" id="'+name+'" name="'+name+'"> \
				</div>';
}


const addButton = (name, label, action, Cssclass) => {
	return '<a class="'+ Cssclass +'" id="'+ name + '" onclick="'+action+'">'+ label+'</a>';
}

const addCustom = (custom) => {
	return custom;
}

const getCurData = () =>{
	var status = true;
	var data = {};
	$.each(globalFields, (key, value) => {
		if(value.validation=='required'){
			var _val = $('#'+ value.name).val();
			if(_val !==''){
				data[value.name] = _val;
			}else{
				status = false;
			}
		}else{
			var _val = $('#'+ value.name).val();
			data[value.name] = _val;
		}
	});
	return {status:status, data:data};
}

const defaultSave = () =>{
	var status = true;
	var data = {};
	$.each(globalFields, (key, value) => {
		if(value.validation=='required'){
			var _val = $('#'+ value.name).val();
			if(_val !==''){
				data[value.name] = _val;
				console.log(value);	
			}else{
				status = false;
			}
		}
	});
	if(status){
		data.sysInsert = Date.now();
		firebase.database().ref(docsCode).push(data);
		$.notify(
			"Selamat kamu telah berhasil terdaftar!", 
			{ 
			className:"success"}
		);
		setTimeout( ()=>{
			location.reload();
		}, 3000);
		
	}else{
		console.log('tidak insert');
	}
}