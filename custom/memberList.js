var userInfo = {};

$(document).ready(()=>{
	
});

var mainApp = angular.module("memberList", []);
mainApp.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
	angular.forEach(items, function(item, key) {
	  var x = 'Regular';
	  item.key = key;
	  if(item.nominal==400000){
		  x = 'Early Bird';
	  }
	  
	  item.keyword = item.namaLengkap +';' +key + ';' + item.email + ';' + item.pengembalaan + ';' + x;
      filtered.push(item);
    });
	filtered.sort(function (a, b) {
		try{
			if(a[field]== undefined){
				a[field] = '';
			}
			if(b[field]== undefined){
				b[field] = '';
			}
			if(a[field].toLowerCase() < b[field].toLowerCase()) return -1;
			if(a[field].toLowerCase() > b[field].toLowerCase()) return 1;
		}catch(e){
			
		}
		
		return 0;
	});
    if(reverse) filtered.reverse();
    return filtered;
  };
});

mainApp.filter('custom', function() {
  return function(input, search) {
	if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
		var status = true;
		angular.forEach(search, function(value2, key2){
			try{
				var actual = value[key2];
				if (!(actual.toString().toLowerCase().indexOf(value2.toString().toLowerCase()) !== -1)) {
					status = false;
				}
			}catch(e){
				status = false;
			}
			
		});
		if(status){
			result[key] = value;
		}
      /*if(value.fullname.toLowerCase()=="Jose"){
		  console.log(actual.indexOf.toLowerCase()(expected));
		}
	  if (actual.toLowerCase().indexOf(expected) !== -1) {
        result[key] = value;
      }*/
    });
    return result;
  }
});


mainApp.controller('listController', function($scope, $filter) {
	$scope.isAdmin = false;
	$scope.access = {};
	$scope.customDate = '';
	$scope.data= [];
	$scope.filterData = {};
	$scope.status = true;
	$scope.showDetail = function(data){
		localStorage.memberInfo = JSON.stringify(data);
		window.location="memberDetail.html";
	};
	
	$scope.id = -1;
	
	$scope.setFilter = function(id){
		if(id!==-1){
			$scope.filterData.statusConf =id;
		}else{
			delete $scope.filterData.statusConf;
		}
		$scope.id = id;
	};
	
	$scope.getResource = function(id){
		var tmp = '../img/default-img.png';
		if(id===0){
			tmp = '../img/new.png';
		}else if(id===1){
			tmp = '../img/waiting.png';
		}else if(id==2){
			tmp = '../img/ok.png';
		}else if(id==3){
			tmp = '../img/No.png';
		}
		return tmp;
	};
	
	$scope.getClass = function(id){
		if($scope.id == id){
			return 'btn btn-primary btn-sm';
		}else{
			return 'btn btn-info btn-sm';
		}
	};
	$scope.isDelete = false;
	$scope.load = function(){
		$('.loading').show();
		
		var ref = firebase.database().ref('OltParticipan');
		ref.once("value", function(snapshot) {
			$scope.data = snapshot.val();
			if($scope.status){
				$scope.$apply();
			}
			$('.loading').hide();
		}, function (error) {
		   console.log("Error: " + error.code);
		});
	};
	$scope.load();
	
	$scope.doDelete = function(key, namaLengkap){
		try{
			var conf = confirm("Apakah anda yakin untuk menghapus data "+ key +" dengan nama peserta "+ namaLengkap+"?");
			if(conf){
				$scope.status= false;
				if(true){
					firebase.database().ref('OltParticipan').child(key).set(null, function(err){
						if(err){
							alert(err)
						}else{
							console.log($scope.data);
							alert('success');
							window.location.reload();
						}
					})
				}
			}else{
				alert('Dibatalkan');
			}
			
		}catch(err){
			alert(err);
		}
	}
	
	$scope.exportData = function(){
		var excel = $JExcel.new();            
		excel.set( {sheet:0,value:"One Life Ticket" } );
		var evenRow=excel.addStyle( { border: "none,none,none,thin #333333"});                                                    
		var oddRow=excel.addStyle ( { fill: "#ECECEC" ,border: "none,none,none,thin #333333"}); 
		 
		var headers=["Nama Lengkap","Alamat","Sekolah","Kelas","Phone Number","Join CG"];                            
		
		for (var i=0;i<headers.length;i++){                       // Loop headers
			excel.set(0,i,0,headers[i]);             // Set CELL header text & header format
			excel.set(0,i,undefined,"auto");                      // Set COLUMN width to auto 
		}
		var i= 1;
		$.each($scope.data, function(key,value){
			
			               
			excel.set(0,1,i,value.namaLengkap);                 
			excel.set(0,2,i,value.alamat);                    
			excel.set(0,3,i,value.sekolah);                        
			excel.set(0,4,i,value.kelas);                    
			excel.set(0,5,i,value.notelepon);                 
			excel.set(0,6,i,value.sudahbercg);                  
			i= i+1;
		});

		excel.generate("onelifeticket_"+ Date.now()+".xlsx");
	};
 });
var access = {};
firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		authId = user.uid;
		firebase.database().ref('users/' + authId).on('value', (snap)=>{
//			if(true)){
				firebase.database().ref('users/' + authId).set({
					email: user.email, 
					fullName: user.displayName,
					phone:user.phoneNumber,
					profileUrl:user.photoURL,
					access:{
						onlife:true
					}
				});
			
		});
	} else {
		window.location ='login.html';
	}
});
