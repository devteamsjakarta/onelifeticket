$(document).ready(()=>{
	$('#logoutBtn').click(()=>{
		logout();
	});
	$('#listBtn').click(()=>{
		window.location="Mlist.html";
	});
	$('#reportBtn').click(()=>{
		window.location="report.html";
	});
});

firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		authId = user.uid;
		firebase.database().ref('users/' + authId).on('value', (snap)=>{
			if(true){
				firebase.database().ref('users/' + authId).set({
					email: user.email, 
					fullName: user.displayName,
					phone:user.phoneNumber,
					profileUrl:user.photoURL,
					access:{
						onlife:false
					}
				});
			}else{
				if(!snap.val().access.onlife){
					window.location = 'wait.html';
				}else{
					
				}
			}
		});
	} else {
		window.location ='login.html';
	}
});

function randomDate(start, end) {
    var d= new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    return d;
}