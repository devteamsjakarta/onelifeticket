// Initialize Firebase
var config = {
	apiKey: "AIzaSyB8pCTEjUjPbSxhWawizULcVSzXT1GxENM",
	authDomain: "i-hub-favour.firebaseapp.com",
	databaseURL: "https://i-hub-favour.firebaseio.com",
	projectId: "i-hub-favour",
	storageBucket: "i-hub-favour.appspot.com",
	messagingSenderId: "1003772088947"
};
firebase.initializeApp(config);

function googleSignIN(){
	var provider = new firebase.auth.GoogleAuthProvider();
	provider.addScope('https://www.googleapis.com/auth/contacts.readonly');

	firebase.auth().signInWithPopup(provider).then(function(result) {
		// This gives you a Google Access Token. You can use it to access the Google API.
		var token = result.credential.accessToken;
		// The signed-in user info.
		var user = result.user;
	}).catch(function(error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		//he email of the user's account used.
		var email = error.email;
		// The firebase.auth.AuthCredential type that was used.
		var credential = error.credential;
	});
}

function logout(){
	firebase.auth().signOut();
}
