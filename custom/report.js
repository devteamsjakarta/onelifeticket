var userInfo = {};

$(document).ready(()=>{
	
});

var mainApp = angular.module("memberList", []);
mainApp.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item, key) {
	  item.key = key;
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
		if(a[field].toLowerCase() < b[field].toLowerCase()) return -1;
		if(a[field].toLowerCase() > b[field].toLowerCase()) return 1;
		return 0;
	});
    if(reverse) filtered.reverse();
    return filtered;
  };
});

mainApp.filter('custom', function() {
  return function(input, search) {
	if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
		var status = true;
		angular.forEach(search, function(value2, key2){
			try{
				var actual = value[key2];
				if (!(actual.toString().toLowerCase().indexOf(value2.toString().toLowerCase()) !== -1)) {
					status = false;
				}
			}catch(e){
				status = false;
			}
			
		});
		if(status){
			result[key] = value;
		}
      /*if(value.fullname.toLowerCase()=="Jose"){
		  console.log(actual.indexOf.toLowerCase()(expected));
		}
	  if (actual.toLowerCase().indexOf(expected) !== -1) {
        result[key] = value;
      }*/
    });
    return result;
  }
});


mainApp.controller('listController', function($scope, $filter) {
	$scope.data = [];
	$scope.dataDate = [];
	$scope.totalUnik = [];
	$scope.statusRegis = [];
	$scope.totalCounter = 0;
	$scope.load = function(){
		$('#overlay').show();
		
		var ref = firebase.database().ref('ycParticipant');
		ref.once("value", function(snapshot) {
			$scope.data = snapshot.val();
			$.each($scope.data, function(key,value){
				/*--START DATA REGIS BY DATE--*/
				var tanggalShow = $filter('date')(value.timeRegis , 'dd-MM-yyyy');
				if($scope.dataDate.filter(function(data){
					return data.tanggal == tanggalShow;
				}).length === 0){
					var tmp = {};
					tmp.tanggal =  tanggalShow ;
					tmp.count = 1;
					$scope.dataDate.push(tmp);
				}else{
					var index = $scope.dataDate.map(x => x.tanggal).indexOf(tanggalShow);
					$scope.dataDate[index].count +=1;
				}
				/*--END DATA REGIS BY DATE--*/
				
				const {email, statusConf} = value;
				
				/*--START DATA REGIS UNIQUE--*/
				if($scope.totalUnik.filter(function(data){
					return data.email == email;
				}).length === 0){
					var tmp = {};
					tmp.email =  email ;
					tmp.count = 1;
					$scope.totalUnik.push(tmp);
				}else{
					var index = $scope.totalUnik.map(x => x.email).indexOf(email);
					$scope.totalUnik[index].count +=1;
				}
				
				/*--END DATA REGIS UNIQUE-*/
				
				/*--START DATA REGIS STATUS--*/
				if($scope.statusRegis.filter(function(data){
					return data.statusID == statusConf;
				}).length === 0){
					var tmp = {};
					tmp.statusID =  statusConf ;
					if(tmp.statusID===0){
						tmp.statusLabel='New / Waiting Payment';
					}else if(tmp.statusID===2){
						tmp.statusLabel='Diterima';
					}else if(tmp.statusID===3){
						tmp.statusLabel='Ditolak';
					}
					tmp.count = 1;
					$scope.statusRegis.push(tmp);
				}else{
					var index = $scope.statusRegis.map(x => x.statusID).indexOf(statusConf);
					$scope.statusRegis[index].count +=1;
				}
				/*--END DATA REGIS UNIQUE-*/
				
				$scope.totalCounter +=1;
				$scope.$apply();
				
			});
			console.log($scope.totalUnik);
			$('#overlay').hide();
		}, function (error) {
		   console.log("Error: " + error.code);
		});
	};
	$scope.load();
 });

firebase.auth().onAuthStateChanged(function(user) {
	if(!user) {
		window.location = 'index.html';
		
	}
});
