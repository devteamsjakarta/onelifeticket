var userInfo = {};

$(document).ready(()=>{
	$('#logoutBtn').click(()=>{
		logout();
	});
});

firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		authId = user.uid;
		firebase.database().ref('users/' + authId).on('value', (snap)=>{
			if(!snap.exists()){
				firebase.database().ref('users/' + authId).set({
					email: user.email, 
					fullName: user.displayName,
					phone:user.phoneNumber,
					profileUrl:user.photoURL,
					access:{
						onlife:false
					}
				});
			}else{
				if(snap.val().access.onlife){
					window.location = 'index.html';
				}
			}
		});
	} else {
		authId = '';
		window.location = "login.html";
	}
});
