'use strict';

 var mainApp = angular.module("listMan", []);
 mainApp.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

mainApp.filter('custom', function() {
  return function(input, search) {
  if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
		var status = true;
		angular.forEach(search, function(value2, key2){
			try{
				var actual = value[key2];
				if (!(actual.toString().toLowerCase().indexOf(value2.toString().toLowerCase()) !== -1)) {
					status = false;
				}
			}catch(e){
				status = false;
			}
			
		});
		if(status){
			result[key] = value;
		}
      /*if(value.fullname.toLowerCase()=="Jose"){
		  console.log(actual.indexOf.toLowerCase()(expected));
		}
	  if (actual.toLowerCase().indexOf(expected) !== -1) {
        result[key] = value;
      }*/
    });
    return result;
  }
});

 mainApp.controller('listController', function($scope, $filter) {
	$scope.customDate = '';
	$scope.data = [];
	$scope.filterData = {};
	$scope.selected = {};
	$scope.showPayment = (data) =>{
		$scope.status =false;
		$scope.selected = data;
		$('#paymentModal').modal('toggle');
	};
	
	$scope.doClose = () =>{
		$('#paymentModal').modal('toggle');
	};
	
	$scope.doApprove = () =>{
		var key = $scope.selected.kodeReg;
		var ref = firebase.database().ref('OltParticipan/' + key);
		ref.update({
			statusConf: 1
		});
		$scope.doClose();
	}
	
	$scope.doReject = () =>{
		var key = $scope.selected.kodeReg;
		var ref = firebase.database().ref('OltParticipan/' + key);
		ref.update({
			statusConf: 2
		});
		$scope.doClose();
	}
	
	$scope.setData = function(row, key){
		$scope.selected.key = key
		$scope.selected.fullname = row.fullname;
		$scope.selected.schoolname = row.schoolname;
		$scope.selected.class = parseInt(row.class);
		$scope.selected.pengantaran = row.pengantaran;
		$scope.selected.hanphpne = parseInt(row.hanphpne);
		if(row.meetingPoint==undefined){
			$scope.selected.meetingPoint = '';
		}else{
			$scope.selected.meetingPoint = row.meetingPoint;
			
		}
		if(row.daftar==undefined){
			$scope.selected.daftar = 'Belum';
		}else{
			$scope.selected.daftar = row.daftar;
		}
	};
	$scope.selectedView = {};
	$scope.showData = function(row){
			$scope.selectedView = row;
	}
	
	$scope.updateData = function(key){
		/*
		ref.update({firstname:"boby lagi 3"});*/
		$scope.status = false;
		var ref = firebase.database().ref('listOfParticipant/' + key);
		ref.update({
			operatorName: $scope.adminName,
			absenStatus: 1,
			absenTime: Date.now()
		});
	};
	$scope.resetData = function(key){
		$scope.status = false;
		var ref = firebase.database().ref('listOfParticipant/' + key);
		ref.update({
			absenStatus: null,
			absenTime: null,
			namaPengantar: null,
			nomor: null,
			telepon: null
			
		});
	};
	$scope.saveChange = function(){
		var ref = firebase.database().ref('listOfParticipant/' + $scope.selected.key);
		ref.update({
			fullname:$scope.selected.fullname,
			schoolname:$scope.selected.schoolname,
			class:$scope.selected.class,
			pengantaran:$scope.selected.pengantaran,
			hanphpne:$scope.selected.hanphpne,
			meetingPoint:$scope.selected.meetingPoint,
			daftar:$scope.selected.daftar
		});
	};
	$scope.status = true;
	
	$scope.load = function(){
		//var ref = firebase.database().ref('listOfTest').orderByChild('pengantaran').equalTo($scope.pengantaran[$scope.dataFilter]);
		var ref = firebase.database().ref('OltParticipan');
		
		ref.on("value", function(snapshot) {
			$scope.data = snapshot.val();
			if($scope.status){
				$scope.$apply();
			}
			console.log($scope.data);
		}, function (error) {
		   console.log("Error: " + error.code);
		});		
	};
	
	$scope.load();
	
 });
 