var img = '';
var name = '';
$(document).ready(()=>{
});

$('#nomorRegistrasi').blur(()=>{
	var tmpId = $('#nomorRegistrasi').val().toString().toUpperCase();
	$('#nomorRegistrasi').val(tmpId);
	var fb = firebase.database().ref("listParticipant/"+tmpId + "/namaLengkap").on('value',(snap)=>{
		name=snap.val();
		$('#nameTxt').html(name);
	});
});

$('#saveBtn').click(()=>{
	doSave();
});

function doClose(){
	$('#exampleModal').modal('toggle');
	location.reload();
}

function doSave(){
	var tmpId = $('#nomorRegistrasi').val();
	var nominal = $('#nominal').val();
	var bank= $('#bank').val();
	var nama= $('#namaPemilik').val();
	var tanggal= $('#tanggalTransfer').val();
	if(tmpId!=='' && bank!=='' &&nama!=='' && tanggal!==''){
		var data = {};
		data.insertData = Date.now();
		data.nominal = nominal;
		data.bank = bank;
		data.nama = nama;
		data.tanggal = tanggal;
		data.img = img;
		var fb = firebase.database().ref("listParticipant/"+tmpId + "/paymentConfirmation");
		fb.push(data, function(err) {
		  console.log(err);
		});
		$.notify(
			"Success submit confirmation payment", 
			{ 
				className:"success"
			}
		);
		$('#exampleModal').modal({backdrop: 'static', keyboard: false});

	}else{
		$.notify(
			"Data tidak lengkap", 
			{ 
				className:"warning"
			}
		);
	}
	
}

$("#files").change(function(e) {
	var folderTmp = $('#nomorRegistrasi').val();
	$('#prog').html(Math.round(0));
	readURL(this);
	var file = e.target.files[0];
	var storageRef = firebase.storage().ref(folderTmp +'/' + file.name);
	var uploadTask = storageRef.put(file);
	uploadTask.on('state_changed', function(snapshot){
	  // Observe state change events such as progress, pause, and resume
	  // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
	  var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
	  
	  $('#prog').html(Math.round(progress));
	  switch (snapshot.state) {
		case firebase.storage.TaskState.PAUSED: // or 'paused'
		  break;
		case firebase.storage.TaskState.RUNNING: // or 'running'
		  break;
	  }
	}, function(error) {
	  // Handle unsuccessful uploads
	}, function() {
	  // Handle successful uploads on complete
	  // For instance, get the download URL: https://firebasestorage.googleapis.com/...
	  var downloadURL = uploadTask.snapshot.downloadURL;
	  $('#saveBtn').removeClass('disabled');
	  img = downloadURL;
	  $("#prev").notify(
			"Success Upload", 
			{ 
			className:"success",
			position:"right" }
		);
	});

	
	
});

var file;
function readURL(input) {
	if (input.files && input.files[0]) {
    var reader = new FileReader();
	reader.onload = function(e) {
      $('#prev').attr('src', e.target.result);
	  file = e.target.result;
    }
	reader.readAsDataURL(input.files[0]);
  }
}