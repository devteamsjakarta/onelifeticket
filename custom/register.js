var total = 0;
$(document).ready(()=>{
	firebase.database().ref('OltParticipan').on('value', function(snap){
		total = snap.numChildren();
		if(total<=150){
			$('[name="ticket"]').val('Early Bird');
			$('#typePaymentGroup').remove();
		}else{
			$('[name="ticket"]').val('Regular');
		}
	});
	$.getJSON( "data/form.json", function( data ) {
		const {fields, title, database} = data;
		globalFields = fields;
		docsCode = database;
		$('#title').html(title);
		$.each(fields, function( key, val ) {
			$('#globalForm').append(createField(val));
		});
		$('[name="ticket"]').attr('readonly','readonly');
		var isEarlyBird = 2;
		var dat1 = new Date(2018,03,09);
		var dat2 = Date.now();
		if((dat1-dat2)>0){
			isEarlyBird  = 1;
		}
		if(isEarlyBird==1 && total <=150){
			$('[name="ticket"]').val('Early Bird');
		}else{
			$('[name="ticket"]').val('Regular');
		}
	
	
		$('.select2').select2({
			 theme: "bootstrap"
		});
		
		$.fn.select2.defaults.set( "theme", "bootstrap" );
		$('#statusCg').on('change', function(){
		if($('#statusCg').val()=='ya'){
			$('#asalCgGroup').show();
			$('#pengembalaanGroup').show();
		}else{
			$('#asalCgGroup').hide();
			$('#pengembalaanGroup').hide();
		}
	});
	});
});


function doClose(){
	$('.bd-example-modal-lg').modal('toggle');
}
function doClose2(){
	$('#globalForm')[0].reset();
	$('.modal-success').modal('toggle');
}

function resent(){
	var email = $('#email').val();
	$.ajax({
		url:"https://us-central1-i-hub-favour.cloudfunctions.net/resentEmailYc?id=" + keyValue,
		method:"get",
		dataType: "json"
	}).done(function(msg){
		$('.modal-failed').modal('toggle');
		alert("Email Sent");
	});
};

var keyValue = '';
function doSave(){

	const {status, data} = getCurData();
	
	if(status){
		data.registime=Date.now();
		delete data.submit;
		firebase.database().ref("OltParticipan").push(data,function(err){
		if(err){}
		else{alert('Success');}
		});
	}
	else
	{
		alert('Data tidak lengkap!');
	}
}

function customSave(){
	$('.bd-example-modal-lg').modal('toggle');
}
